#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <cmath>
using namespace std;

const int sizes[] = {80, 160, 91, 64, 96, 336, 21};
const int startIndexes[] = {0xF600, 0xF300, 0xF3A0, 0xF400, 0xF440, 0xF4A0, 0xF58F};
int setSelected;
int idSelected;
int skinTone;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    resetEmoji(0);

    setFixedSize(size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resetEmoji(unsigned int id) {
    id += startIndexes[setSelected];

    QString str = getStringForID(id);
    if(skinTone != 0) {
        str += getStringForID(0xF3FA + skinTone);
    }

    ui->emojioutput->setPlainText(str);
}

QString MainWindow::getStringForID(unsigned int id) {
    // oh, the pain!
    // see https://en.wikipedia.org/wiki/UTF-16

    ui->hexcode->display((int) id);

    // we will use this later
    uint lowerBits = 10;
    uint lowerMask = pow(2, lowerBits) - 1;

    // upper and lower 10-bit chunks
    uint lower = id & lowerMask;
    uint upper = id>>lowerBits;

    // upper and lower surrogate - see wikipedia examples
    uint upperSug = upper + 0xD800;
    uint lowerSug = lower + 0xDC00;

//    // print everything
//    cout << "id: " << id << endl;
//    cout << "upper: " << upperSug << endl;
//    cout << "lower: " << lowerSug << endl;

    QString str;
    str += QChar(upperSug);
    str += QChar(lowerSug);
    return str;
}

void MainWindow::on_spinBox_valueChanged(int id) {
    idSelected = id - 1;
    resetEmoji(idSelected);
}

void MainWindow::on_comboBox_currentIndexChanged(int index) {
    ui->spinBox->setValue(1);
    ui->spinBox->setMaximum(sizes[index]);
    setSelected = index;
    resetEmoji(0);
}

void MainWindow::on_comboBox_2_currentIndexChanged(int index) {
    skinTone = index;
    resetEmoji(idSelected);
}

void MainWindow::on_addEmoji_clicked() {
    QString str = ui->emojioutput->toPlainText();
    ui->largeoutput->insertPlainText(str);
}
